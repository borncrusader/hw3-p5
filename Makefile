p5: mytest.c mysched.o myqueue.o mythread-new3.a
	gcc -g mytest.c mythread-new3.a mysched.o myqueue.o -o p5

mysched.o: mysched.c
	gcc -c -g mysched.c -o mysched.o

myqueue.o: myqueue.c
	gcc -c -g myqueue.c -o myqueue.o

clean:
	rm -rf mysched.o myqueue.o p5
