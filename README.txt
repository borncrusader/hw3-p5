

mythread_init_sched():
	This function will be called everytime mythread_create() function was invoked. We register the signal handler for handling the signals SIGUSR1 and SIGALRM. Also we use the settimer() function to send the SIGALRM signal every 10 milli-seconds. Use a static variable to make sure that actions taken inside mythread_init_sched() are not repeated during every invocation. 

mythread_exit_sched():
	This function will be called whenever the thread exits. We unregister the signal handlers that we had set earlier. 

mythread_leave_kernel():
	mythread_leave_kernel invokes the non-preemptive version of the leave kernel call and then checks if the thread is currently preempted. This is to ensure that the invocations to mythread_leave_kernel() from inside mythread_block() does not trigger scheduler. We check if the reschedule flag is set in any of the threads in the run queue. If the flag is set, we invoke the scheduler and proceed to block if the scheduler returns 1. 

mythread_scheduler(); 
	The scheduler's job is to find if there is a thread with a runnable priority at ready queue given the priorities of threads in the run queue. If yes, the scheduer sends 1 else it returns 0. The mythread_scheduler() function is called when a thread handles SIGALRM or SIGUSR1. It is also called from mythread_leave_kernel() when the reschedule flag is set in the TCB. The scheduler sends SIGUSR1 to all the threads in the run queue when it was invoked while handling SIGALRM. Then it compares the scheduling priority of the current thread and the and highest priority thread in the ready queue and if the thread in ready queue is schedulable, it returns 1. In response, the caller function immediately calls mythread_block() to trigge to schedule a new thread to run. 


an internal signal handler(): 
	On recieving the SIGALRM , we try to enter the kernel and invoke the mythread_scheduler and call mythread_block if the scheduler returns 1. In addition we also set a state flag to indicate that the thread has received the SIGALRM signal. If we unable enter the kernel, we set the reschedule flag. Presense of this flag is tested inside the mythread_leave_kernel(). 
	
	on receipt of SIGUSR1, we again enter the kernel and invoke the scheduler and call the mythread_block() if the scheduler returns 1. 

mythread_attr_init():
	In mythread_attr_init() we initialize the scheduling attribute variable to the default value. 

mythread_attr_destroy():
	In mythread_attr_destroy() we initialize the scheduling attribute variable to the default value. 

