#include <stdlib.h>
#include "myqueue.h"
#include "mythread.h"

//#define DEBUG

void mythread_qprint(mythread_queue_t *headp);

void mythread_q_init(mythread_queue_t *headp, void *item)
{
  if (!headp)
    return;

  if (*headp == NULL) {
    /* just call the enq function here! */
    mythread_enq(headp, item);
  }
#ifdef DEBUG
  printf("q init called, q is %p\n", (*headp));
#endif
}

int mythread_inq(mythread_queue_t *headp, void *item)
{
  mythread_queue_t tp;

  if (!headp || !(*headp))
    return FALSE;

#ifdef DEBUG
  printf("inq called, q is %p\n", *headp);
#endif

  tp = *headp;
  do {
    if (tp->item == item) {
      return TRUE;
    }
    tp = tp->next;
  } while (tp != NULL);

  return FALSE;
}

/*
 * can be called with run q - where there could be elements
 * or with ready q - where there cannot be any elements
 */
void mythread_enq(mythread_queue_t *headp, void *item)
{
  mythread_t tcb;
  mythread_queue_t tp;
  mythread_queue_t newp;
  int new_attr = DEFAULT_ATTR;
  int old_attr = DEFAULT_ATTR;

#ifdef DEBUG
  printf("enqueue called, q is %p, ", *headp);
#endif
  if (!headp || !item)
    return;

  newp = malloc(sizeof(struct mythread_queue));
  if (!newp) {
    fprintf(stderr, "malloc failed in enqueue!\n");
    exit(1);
  }
  newp->item = item;
  newp->next = NULL;
  newp->prev = NULL;

  tcb = item;
  if (tcb->attribute)
    new_attr = tcb->attribute->attr;

  if (*headp == NULL) {
    /* empty queue */
    *headp = newp;
  } else {
    ///* plain enqueue */
    //tp = *headp;
    //while (tp->next != NULL) {
    //  tp = tp->next;
    //}
    //tp->next = newp;
    //newp->prev = tp;
 
    /* priority based enqueue */
    tp = *headp;
    while (1) {
      tcb = tp->item;
      old_attr = DEFAULT_ATTR;
      if (tcb->attribute)
        old_attr = tcb->attribute->attr;
      if (new_attr < old_attr) {
        /* first thread with greater priority
         * insert to this thread's front */
        break;
      }
      if (tp->next == NULL) {
        /* end of the queue */
        break;
      }
      tp = tp->next;
    }

    if (new_attr < old_attr) {
      /* push to the front */
      newp->prev = tp->prev;
      newp->next = tp;
      if (tp == *headp) {
        /* tp was the head */
        *headp = newp;
      } else {
        tp->prev->next = newp;
      }
      tp->prev = newp;
    } else {
      /*append to the back */
      newp->prev = tp;
      newp->next = tp->next;
      if (tp->next != NULL) {
        /* trailing node */
        tp->next->prev = newp;
      }
      tp->next = newp;
    }
  }

#ifdef DEBUG
  printf("new elem is %p\n", newp);
#endif
}

void mythread_deq(mythread_queue_t *headp, void *item)
{
  mythread_queue_t tp;

  if (!headp || !(*headp))
    return;

#ifdef DEBUG
  printf("dequeue called, q is %p, ", *headp);
#endif

  tp = *headp;
  do {
    if (tp->item == item) {
      break;
    }
    tp = tp->next;
  } while (tp != NULL);

  if (tp) {
    if (tp == *headp) {
      /* removing head */
      *headp = tp->next;
    }
    if (tp->next) {
      tp->next->prev = tp->prev;
    }
    if (tp->prev) {
      tp->prev->next = tp->next;
    }
#ifdef DEBUG
    printf("removed %p, ", tp);
#endif
    free(tp);
  }
#ifdef DEBUG
  printf("deq after q is %p\n", *headp);
#endif
}

void *mythread_deq_prio(mythread_queue_t *headp)
{
  if (!headp || !(*headp))
    return NULL;

#ifdef DEBUG
  printf("dequeue-prio called\n");
#endif

  return (*headp)->item;
}

void mythread_qprint(mythread_queue_t *headp)
{
  mythread_queue_t tp;
  mythread_t item;
  int item_attr = DEFAULT_ATTR;

  if (!headp || !(*headp))
    return;

  tp = *headp;
  do {
    item = tp->item;
    if (item->attribute)
      item_attr = item->attribute->attr;
#ifdef DEBUG
    printf("(%d,%d)->", item->tid, item_attr);
#endif
    tp = tp->next;
  } while (tp != NULL);
#ifdef DEBUG
  printf("NULL\n");
#endif
}
