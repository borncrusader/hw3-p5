#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>
#include <sys/syscall.h>

#include "mythread.h"
#include "mysched.h"

#define TIME 999999
//#define TIME 10000

#define PREEMPTED 0x8000
#define ALRMCLD 0x4000
#define USR1CLD 0x2000

static pid_t gettid(void)
{
  return (pid_t) syscall(SYS_gettid);
}

static long tgkill(int tgid, int pid, int sig)
{
  return (long) syscall(SYS_tgkill, tgid, pid, sig);
}

static void mythread_set_signal_handler(void)
{
  struct sigaction act;

  memset(&act, 0, sizeof(act));
  act.sa_handler = NULL;
  if(sigemptyset(&act.sa_mask) ||
     sigaddset(&act.sa_mask, SIGUSR1) ||
     sigaddset(&act.sa_mask, SIGALRM)) {
    fprintf(stderr, "sigmask failed!\n");
    exit(1);
  }
  act.sa_sigaction = mythread_sighandler;
  act.sa_flags = SA_SIGINFO|SA_RESTART;
  if (sigaction(SIGUSR1, &act, NULL) ||
      sigaction(SIGALRM, &act, NULL)) {
    fprintf(stderr, "signal handler couldn't be registered!\n");
    exit(1);
  }
}

static void mythread_unset_signal_handler(void)
{
  sigset_t set;

  if(sigemptyset(&set) ||
     sigaddset(&set, SIGUSR1) ||
     sigaddset(&set, SIGALRM)) {
    fprintf(stderr, "sigmask failed!\n");
    exit(1);
  }
  sigprocmask(SIG_BLOCK, &set, NULL);
}

static int mythread_compare_priorities(mythread_t run, mythread_t ready)
{
  int run_attr = DEFAULT_ATTR;
  int ready_attr = DEFAULT_ATTR;

  if (run->attribute) {
    run_attr = run->attribute->attr;
  }
  if (ready->attribute) {
    ready_attr = ready->attribute->attr;
  }

  if (ready_attr <= run_attr) {
    return 1;
  }

  return 0;
}

static void mythread_send_sigusr1()
{
  mythread_t self;
  mythread_t run_curr;
  mythread_t ready_first;
  mythread_queue_t *runq;

  mythread_queue_t tp;
  int pid = getpid();

  self = mythread_self();
  runq = mythread_runq();
  ready_first = mythread_deq_prio(mythread_readyq());

  tp = *runq;
  assert(tp != NULL); // run queue cannot be NULL
  do {
    run_curr = tp->item;
    //assert(run_curr->tid != 0);

    if (run_curr->tid != 0) {
      if (run_curr != self && mythread_compare_priorities(run_curr, ready_first)) {
#ifdef DEBUG
        printf("sending SIGUSR1 to %d\n", run_curr->tid);
#endif
        if (tgkill(pid, run_curr->tid, SIGUSR1) == -1) {
          fprintf(stderr, "tid : %d, %s\n", run_curr->tid, strerror(errno));
        }
      }
    }

    tp = tp->next;
  } while(tp != NULL);
}

/* will be inside kernel when this happens
 * see if there is any thread in the readyq
 * whether we want to preempt or not will
 * be taken care of in the SIGUSR1 handling
 *
 * returns 1 - if need to schedule
 * returns 0 - if no need to schedule
 */
int mythread_scheduler(void)
{
  mythread_t self;
  mythread_t ready_first;
  mythread_queue_t *runq;
  mythread_queue_t *readyq;
  int ret = 0;
  
  runq = mythread_runq();
  self = mythread_self();
  readyq = mythread_readyq();

  if (!*readyq) {
    self->state &= ~ALRMCLD;
    self->state &= ~USR1CLD;
    return 0;
  }

  if (!(self->state & USR1CLD)) {
    /* don't send SIGUSR1 if scheduler was called as
     * part of handling SIGUSR1
     */
    mythread_send_sigusr1();
  }

  ready_first = mythread_deq_prio(readyq);

  if (mythread_inq(runq, self) && mythread_compare_priorities(self, ready_first)) {
    ret = 1;
  }

  self->state &= ~ALRMCLD;
  self->state &= ~USR1CLD;

  return ret;
}

void mythread_sighandler(int sig, siginfo_t *siginfo, void *ucp)
{
  int ret;
  mythread_t self = mythread_self();

  if (sig == SIGUSR1) {
    /* SIGUSR1 received */
#ifdef DEBUG
    write(1, "usr1!\n", 6);
#endif
    mythread_enter_kernel();
    mythread_qprint(mythread_runq());
    mythread_qprint(mythread_readyq());
    self->state |= USR1CLD;
    if (mythread_scheduler()) {
      mythread_block(mythread_readyq(), PREEMPTED);
      self->state &= ~PREEMPTED;
      return;
    }
    mythread_leave_kernel_nonpreemptive();
  } else if (sig == SIGALRM) {
    /* SIGALRM received */
#ifdef DEBUG
    write(1, "alrm!\n", 6);
    printf("%d\n", self->tid);
#endif
    mythread_qprint(mythread_runq());
    mythread_qprint(mythread_readyq());

    if (mythread_tryenter_kernel()) {
      self->state |= ALRMCLD;
      if (mythread_scheduler()) {
        mythread_block(mythread_readyq(), PREEMPTED);
        self->state &= ~PREEMPTED;
        return;
      }
      mythread_leave_kernel_nonpreemptive();
    } else {
#ifdef DEBUG
      write(1, "gosh!\n", 6);
#endif
      self->reschedule = 1;
    }
  }
}

void mythread_leave_kernel(void)
{
  mythread_t self;
  mythread_queue_t *runq;
  mythread_queue_t *readyq;
  mythread_queue_t tp;
  mythread_t tcb;
  int reschedule = 0;

  runq = mythread_runq();
  readyq = mythread_readyq();
  self = mythread_self();

  if (self->state & PREEMPTED) {
    mythread_leave_kernel_nonpreemptive();
    return;
  }

  tp = *runq;
  assert(tp != NULL);
  do {
    tcb = tp->item;

    if (tcb->reschedule) {
      tcb->reschedule = 0;
      reschedule = 1;
    }
    tp = tp->next;
  } while(tp != NULL);

  if (reschedule) {
    if(mythread_scheduler()) {
      mythread_block(readyq, PREEMPTED);
      self->state = ~PREEMPTED;
      return;
    }
  }

  mythread_leave_kernel_nonpreemptive();
}

void mythread_init_sched(void)
{
  mythread_t self;
  static int __sched_once = 0;

  struct itimerval timval;

  if (__sched_once == 0) {
    /* guard needed to prevent stray calls
     * of init_sched for re-registering
     */
    __sched_once = 1;
#ifdef DEBUG
    write(1,"init sched!\n", 12);
#endif

    mythread_set_signal_handler();

    timval.it_value.tv_sec = 0;
    timval.it_value.tv_usec = TIME;
    timval.it_interval.tv_sec = 0;
    timval.it_interval.tv_usec = TIME;
    if (setitimer(ITIMER_REAL, &timval, NULL)) {
      fprintf(stderr, "setitimer failed!\n");
      exit(1);
    }

    /* set main thread's TID! */
    //self = mythread_self();
    //assert(self->tid == 0);
    //self->tid = getpid();
  }
}

void mythread_exit_sched(void)
{
#ifdef DEBUG
  write(1,"exit!\n", 6);
#endif
  mythread_unset_signal_handler();
}

int mythread_attr_init(mythread_attr_t *attr)
{
  if (!attr)
    return EINVAL;

  attr->attr = DEFAULT_ATTR;

  return 0;
}

int mythread_attr_destroy(mythread_attr_t *attr)
{
  if (!attr)
    return EINVAL;

  attr->attr = DEFAULT_ATTR;

  return 0;
}

int mythread_attr_getschedparam(const mythread_attr_t *attr,
		struct sched_param *param)
{
  if (!attr || !param)
    return EINVAL;

  param->sched_priority = attr->attr;

  return 0;
}

int mythread_attr_setschedparam(mythread_attr_t *attr,
		const struct sched_param *param)
{
  if (!attr || !param)
    return EINVAL;

  attr->attr = param->sched_priority;

  return 0;
}

/* mythread_init_sched
 *   init signal handler
 *   setitimer
 *   don't do it for every thread
 *   as all threads will inherit main's signal handler
 *
 * mythread_exit_sched
 *   sigprocmask SIGUSR1 and SIGALRM
 *
 * signal handler
 *   SIGALRM
 *   can be received by any thread in any q
 *     try enter kernel
 *     if fails
 *       set my reschedule flag
 *     if succeeds
 *       call scheduler
 *         check if readyq is not empty
 *         check threads in the runq that are schedulable
 *           send SIGUSR1 to them, except to me if i'm there
 *         check if current thread is in runq
 *       check output of scheduler
 *         block on readyqueue!
 *           internally calls leave_kernel()
 *       else
 *         leave kernel non-preemptively
 *   SIGUSR1
 *   technically can be received by thread in runq
 *   he can only preempt himself,
 *   so scheduler's isrunqthread check should pass
 *     enter kernel
 *     i was informed that there is a thread with lesser priority
 *     might not be true! so call scheduler again
 *     check reschedule flag,
 *       reset it
 *       block on readyqueue!
 *     else
 *       leave_kernel non-preemptively
 *
 * leave kernel
 *   can be called by any thread in anyq
 *   can be called by block called by the signal handler
 *     shouldn't invoke scheduler then
 *
 *   check if any thread in runq has reschedule set
 *   if yes
 *     invoke scheduler
 *   leave_kernel non-preemptively
 */
