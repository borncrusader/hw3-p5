#include <stdio.h>

#include "mythread.h"
#include "mymutex.h"

mythread_mutex_t mutex;
int temp;

void* thr(void *args)
{
  int t = 0;
  mythread_t self = mythread_self();
  while(1) {
    t++;
    mythread_mutex_lock(&mutex);
    temp++;
    printf("thread %ld, %d, %d\n", (long)args, self->tid, temp);
    if ((long)args == 5) {
      //write(1, "howdy!\n", 7);
    }
    mythread_mutex_unlock(&mutex);
    if (t == 5) break;
    
    //usleep(10000);
    sleep(1);
  }
}

int main(int argc, char *argv[])
{
  mythread_t myth[10];
  mythread_attr_t *attr;
  struct sched_param sched;
  long i;

  mythread_setconcurrency(1);

  mythread_mutex_init(&mutex, NULL);

  sched.sched_priority = 0;

  for (i=0; i<10; i++) {
    attr = malloc(sizeof(mythread_attr_t));
    mythread_attr_init(attr);
    if (i<5) {
      mythread_attr_setschedparam(attr, &sched);
    }
    mythread_create(&myth[i], attr, thr, (void*)i);
  }

  //mythread_exit(NULL);

  for (i=0; i<10; i++) {
    mythread_join(myth[i], NULL);
  }

  mythread_mutex_destroy(&mutex);

  return 0;
}
